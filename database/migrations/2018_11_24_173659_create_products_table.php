<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'products', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->integer( 'brand_id' );
			$table->string( 'sku' )->unique();
			$table->string( 'name' );
			$table->string( 'slug' )->unique();
			$table->float( 'regular_price' );
			$table->float( 'sale_price' )->nullable();
			$table->text( 'checkout_url' );
			$table->text( 'parse_url' );
			$table->text( 'description' )->nullable();
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'products' );
	}
}
