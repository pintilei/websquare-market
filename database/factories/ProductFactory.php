<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define( App\Models\Product::class, function ( Faker $faker ) {

	$name  = $faker->firstNameFemale . '-' . rand(1,100);
	$price = rand( 100, 999 );

	return [
		'brand_id'      => \App\Models\Brand::all()->random()->id,
		'sku'           => $faker->ipv6,
		'name'          => $name,
		'slug'          => str_slug( $name ),
		'regular_price' => $price,
		'sale_price'    => $price - rand( 50, 75 ),
		'checkout_url'  => $faker->url,
		'parse_url'     => $faker->url,
		'description'   => $faker->text

	];
} );
