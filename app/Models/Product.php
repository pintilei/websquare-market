<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
	protected $fillable = [
		'title',
		'sku',
		'title',
		'regular_price',
		'sale_price',
		'checkout_url',
		'parse_url'
	];

	public function brand() {
		return $this->belongsTo( Brand::class );
	}
}
