<h1>Brand: {{ $brand->name }} ( {{ $products->total() }} products )</h1>
<ul>
    @foreach($products as $product)
        <li><a href="#{{ $product->slug }}">{{ $product->name  }}</a></li>
    @endforeach
</ul>


{{ $products->links() }}