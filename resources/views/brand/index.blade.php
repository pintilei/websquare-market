<h1>{{ $brands->total() }} brands</h1>
<ul>
    @foreach($brands as $brand)
        <li><a href="{{ route('brand.show', ['slug' => $brand->slug]) }}">{{ $brand->name  }}</a></li>
    @endforeach
</ul>


{{ $brands->links() }}